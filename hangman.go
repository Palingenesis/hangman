package main


import(
	"fmt"
	"bufio"
	"io/ioutil"
	"strings"
	"os"
	"time"
	"math/rand"
)

func createGuessWord(sword string) string{
	guessword := ""
	for i := 0; i<len(sword); i++{
		guessword += "_"
	}
	return guessword
}

func checkSecretWord(gword string, sword string, letter string) (bool,string){
	containsletter := false
	swordR := []rune(sword)    /*Strings are immutable so I have to convert to runes */
	gwordR := []rune(gword)
	for i, l := range swordR{
		if string(l) == letter{
			gwordR[i] = swordR[i]
			containsletter = true
		}
	}
	return containsletter,string(gwordR)
}

func prompt(vals ...interface{}) (string,error){
	if len(vals) != 0{
		fmt.Println(vals...)
	}
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	err := scanner.Err()
	if err != nil {
		return "",err
	}
	s := scanner.Text()
	return s,err
}

func getSecretWord(filename string)(string){
	filebytes,err := ioutil.ReadFile(filename)
	if(err != nil){
		panic(err)
	}
	filestring := string(filebytes)
	words:= strings.Split(filestring, "\n")
	seed := rand.NewSource(time.Now().UnixNano())
    rand1 := rand.New(seed)
	return words[rand1.Intn(len(words))]
}



var guessletter = ""
func main(){
	//secretword := os.Args[1]
	secretword := getSecretWord(os.Args[1])
	if len(os.Args) != 2 {
		fmt.Println("Error. Enter 1 word to guess")
		return
	}
	guessword := createGuessWord(secretword)
	guesses := len(secretword)
	for guesses>0{
		fmt.Println("Found letters: " + guessword)
		fmt.Println(guesses)
		guessletter = ""
		for true{
			guessletter1,err1 := prompt("Enter a letter to guess: ")
			guessletter = guessletter1   //Go is weird so I have to do this janky stuff.
			if err1 != nil{
				fmt.Println(err1)
			} else if len(guessletter) != 1{
				fmt.Println("Error. Input was not a single letter.")
			} else{
				break
			}
		}
		letterfound,guessword1 := checkSecretWord(guessword,secretword,guessletter)
		guessword = guessword1
		if !letterfound {
			guesses -= 1
		} else if guessword == secretword{
			fmt.Println("You win! The word was: " + secretword)
			break
		}
	}
	if guesses == 0{
		fmt.Println("No more guesses left. You lose. The words was: " + secretword)
	}
}
